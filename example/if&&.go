package main

import "fmt"

func main() {
	var age = 22

	if age > 19 && age < 30 {
		fmt.Println("청년입니다")
	} else if age > 50 || age < 19 {
		fmt.Println("이제 시작이다")
	} else {
		fmt.Println("꿈을 이룰 나이")
	}
}
