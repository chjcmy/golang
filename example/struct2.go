package main

import "fmt"

type House1 struct {
	s string
	d int
	l float64
	t string
}

func main() {
	var house House1 = House1{
		"서울시 마포구 오삼동",
		28,
		9.80,
		"아파트",
	}
	fmt.Println(house)
}
