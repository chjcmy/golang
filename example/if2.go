package main

import "fmt"

func main() {
	air := 32

	if air > 20 {
		fmt.Println("에어컨을 킨다")
	} else {
		fmt.Println("에어컨을 끈다")
	}
}
