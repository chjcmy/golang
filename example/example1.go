package main

import "fmt"

func Multiple(a, b int) int {

	result := a * b

	return result
}

func main() {
	fmt.Println(Multiple(1, 2))
}
