package main

import "fmt"

func main() {
	nums := [...]int{1, 3, 5, 10}

	nums[2] = 300

	for i := 0; i < len(nums); i++ {
		fmt.Println(nums[i])
	}
}
