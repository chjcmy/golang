package main

import "fmt"

func main() {
	temp := 0

	switch true {
	case temp < 10 && temp > 30:
		fmt.Println("바깥 활동하기 좋은 날씨가 아닙니다")
	case temp >= 10 && temp < 30:
		fmt.Println("약간 추울수 있으니 가벼운 겉옷 가져가세요")
	case temp >= 15 && temp < 25:
		fmt.Println("야외 활동하기 좋은 날씨네여")
	default:
		fmt.Println("땃땃한 날씨 입니다")
	}
}
