package main

import "fmt"

var cnt int = 0

func main() {
	if false && IncreaseAndReturn() < 5 {
		fmt.Println("1 증가")
	}
}

func IncreaseAndReturn() int {
	fmt.Print("IncreaseAndReturn()", cnt)
	cnt++
	return cnt
}
