package main

import "fmt"

func getMyAge() (int, bool) {
	return 28, true
}

func main() {

	if age, ok := getMyAge(); ok && age > 20 {
		fmt.Println("너는 너무 어려", age)
	} else if ok && age < 30 {
		fmt.Println("너는 어른이야")
	} else {
		fmt.Println("꿈을 이루자")
	}
}
