package main

import "fmt"

func main() {
	var a int = 10
	var b int
	var c = 5
	d := 3

	fmt.Println(a, b, c, d)
}
